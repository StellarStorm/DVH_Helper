"""
Read and extract DVH structures and metrics from a text file exported by
Eclipse.

This program determines all possible structures by reading all structures
defined in a single file. If multiple files are selected, the program will only
read the first file to determine structures (although it will read through all
files when actually extracting the metrics for each structure.) Therefore, if
multiple files are chosen, it's important that they all be variations of the
same plan so that all structures are identical.

Some metrics are hard-coded and may not exist depending on what was exported.
These are:
Volume, Dose Cover, Sampling Cover, Minimum Dose, Maximum Dose, Mean Dose,
Modal Dose, Median Dose, Standard Deviation, Equivalent Spherical Diameter,
Conformity Index, and Gradient Measure.

Other metrics are defined by the user in the UI: dose at relative volume (up to
5 volumes may be defined), volume at absolute dose (up to 5 absolute doses may
be defined), and volume at relative dose (up to 5 doses may be defined).
"""

# Copyright (C) 2018 S. Gay and licensed under the MIT license.

import ctypes
from datetime import datetime
import os
from platform import platform, system
import sys
import numpy as np
import pandas as pd
from PyQt5 import QtWidgets, QtGui
from DVHui import Ui_MainWindow
from utils import results_book, readText


class Gui(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.ui.pushButton_fileLocation.clicked.connect(self.getFile)
        self.ui.pushButton_extract.clicked.connect(self.extractMetrics)
        self.ui.pushButton_copyDRV.clicked.connect(self.copyDoseStructs)
        self.ui.pushButton_copyVAD.clicked.connect(self.copyAVolStructs)
        self.ui.pushButton_copyVRD.clicked.connect(self.copyRVolStructs)

        self.pwd = os.getcwd()
        self.allMetNames = []
        self.fileList = []

        os.makedirs('Results', exist_ok=True)

    def getFile(self):
        """Get a list of files that contain DVH metrics."""
        self.fileList = QtWidgets.QFileDialog.getOpenFileNames(
            self, 'Select files')[0]
        # Values should be the same for all files in the list, so pick the first
        vals = readText(self.fileList[0])
        structs = list(sorted(vals.keys(), key=str.lower))
        structs.remove('Patient Name')
        structs.remove('Patient ID')
        structs.remove('Plan')
        # Get list of the specific names of all metrics
        self.allMetNames = np.array(list(vals[structs[0]].keys()))
        # Display list of all structures
        self.ui.listWidget_allStructs.addItems(structs)

    def copyDoseStructs(self):
        """
        Copy all structures from the first dose-at-relative volume list widget
        to the other widgets, one at at time. Useful when finding multiple doses
        (i.e. D98 and D95) for the same structures.
        """
        initialStructs = [
            self.ui.listWidget_doseRelVol1.item(i).text()
            for i in range(self.ui.listWidget_doseRelVol1.count())
        ]

        otherStructWidgets = [
            self.ui.listWidget_doseRelVol2, self.ui.listWidget_doseRelVol3,
            self.ui.listWidget_doseRelVol4, self.ui.listWidget_doseRelVol5
        ]

        otherWidgeContents = [
            [
                self.ui.listWidget_doseRelVol2.item(i).text()
                for i in range(self.ui.listWidget_doseRelVol2.count())
            ],
            [
                self.ui.listWidget_doseRelVol3.item(i).text()
                for i in range(self.ui.listWidget_doseRelVol3.count())
            ],
            [
                self.ui.listWidget_doseRelVol4.item(i).text()
                for i in range(self.ui.listWidget_doseRelVol4.count())
            ],
            [
                self.ui.listWidget_doseRelVol5.item(i).text()
                for i in range(self.ui.listWidget_doseRelVol5.count())
            ]
        ]

        # Copy structures from first widget into first empty widget
        for i, contents in enumerate(otherWidgeContents):
            if not contents:
                widget = otherStructWidgets[i]
                widget.addItems(initialStructs)
                break

    def copyAVolStructs(self):
        """
        Copy all structures from the first volume-at-absolute-dose list widget
        to the other widgets, one at at time. Useful when finding multiple
        volumes for the same structures.
        """
        initialStructs = [
            self.ui.listWidget_volAbsDose1.item(i).text()
            for i in range(self.ui.listWidget_volAbsDose1.count())
        ]

        otherStructWidgets = [
            self.ui.listWidget_volAbsDose2, self.ui.listWidget_volAbsDose3,
            self.ui.listWidget_volAbsDose4, self.ui.listWidget_volAbsDose5
        ]

        otherWidgeContents = [
            [
                self.ui.listWidget_volAbsDose2.item(i).text()
                for i in range(self.ui.listWidget_volAbsDose2.count())
            ],
            [
                self.ui.listWidget_volAbsDose3.item(i).text()
                for i in range(self.ui.listWidget_volAbsDose3.count())
            ],
            [
                self.ui.listWidget_volAbsDose4.item(i).text()
                for i in range(self.ui.listWidget_volAbsDose4.count())
            ],
            [
                self.ui.listWidget_volAbsDose5.item(i).text()
                for i in range(self.ui.listWidget_volAbsDose5.count())
            ]
        ]

        # Copy structures from first widget into first empty widget
        for i, contents in enumerate(otherWidgeContents):
            if not contents:
                widget = otherStructWidgets[i]
                widget.addItems(initialStructs)
                break

    def copyRVolStructs(self):
        """
        Copy all structures from the first volume-at-relative-dose list widget
        to the other widgets, one at at time. Useful when finding multiple
        volumes for the same structures.
        """
        initialStructs = [
            self.ui.listWidget_volRelDose1.item(i).text()
            for i in range(self.ui.listWidget_volRelDose1.count())
        ]

        otherStructWidgets = [
            self.ui.listWidget_volRelDose2, self.ui.listWidget_volRelDose3,
            self.ui.listWidget_volRelDose4, self.ui.listWidget_volRelDose5
        ]

        otherWidgeContents = [
            [
                self.ui.listWidget_volRelDose2.item(i).text()
                for i in range(self.ui.listWidget_volRelDose2.count())
            ],
            [
                self.ui.listWidget_volRelDose3.item(i).text()
                for i in range(self.ui.listWidget_volRelDose3.count())
            ],
            [
                self.ui.listWidget_volRelDose4.item(i).text()
                for i in range(self.ui.listWidget_volRelDose4.count())
            ],
            [
                self.ui.listWidget_volRelDose5.item(i).text()
                for i in range(self.ui.listWidget_volRelDose5.count())
            ]
        ]

        # Copy structures from first widget into first empty widget
        for i, contents in enumerate(otherWidgeContents):
            if not contents:
                widget = otherStructWidgets[i]
                widget.addItems(initialStructs)
                break

    def getStructsAndMets(self):
        """
        Find structures and metrics to extract from UI boxes. Note that this
        doesn't actually find the values for the metrics, just a dictionary
        containing the metrics to be found in self.extractMetrics().

        :return allMets: a dictionary of structures and metrics as follows:
            (a). Top-level keys are structures
            (b). Each structure also has a dictionary of at least 'Globals', a
                 list of things like max dose that are found for all structures
                 and don't have an associated metrics.
                 There may be up to three additional keys:
                 1. DoseRelVol: a list of dose to relative volume metrics,
                    i.e. D90%. Multiple metrics can be defined.
                 2. VolAbsDose: A list of volume to absolute dose metrics,
                    i.e. V45Gy. Multiple metrics can be defined.
                 3. VolRelDose: A list of volume to relative dose metrics,
                    i.e. V75%. Multiple metrics can be defined.
                These three other keys, if they exist, may vary per structure.
            Example:
                {'BODY': {'Globals': ['Mean Dose'], 'DoseRelVol': [90.0, 75.0]},
                'Artifact': {'Globals': ['Mean Dose']}}
        """
        # First up are "global" metrics - things like max dose that will be
        # found for all desired structures
        boxes = [
            self.ui.checkBox_volume.isChecked(),
            self.ui.checkBox_doseCover.isChecked(),
            self.ui.checkBox_samplingCover.isChecked(),
            self.ui.checkBox_minDose.isChecked(),
            self.ui.checkBox_maxDose.isChecked(),
            self.ui.checkBox_meanDose.isChecked(),
            self.ui.checkBox_modalDose.isChecked(),
            self.ui.checkBox_medianDose.isChecked(),
            self.ui.checkBox_sd.isChecked(),
            self.ui.checkBox_equivSphereDiam.isChecked(),
            self.ui.checkBox_conformIndex.isChecked(),
            self.ui.checkBox_gradMeasure.isChecked()
        ]

        # Get a list of all wanted global mets. The first 4 items in
        # self.allMetNames are skipped since they're things like approval status
        globalMets = self.allMetNames[4:][np.where(boxes)[0]].tolist()

        finalStructs = [
            self.ui.listWidget_wantedStructs.item(i).text()
            for i in range(self.ui.listWidget_wantedStructs.count())
        ]

        # From here on we're using optional metrics that may only be wanted for
        # a subset of structures -- for instance, D90 for brainstem but not for
        # the eyes.
        doseRelVolValues = [
            self.ui.lineEdit_doseRelVol1.text(),
            self.ui.lineEdit_doseRelVol2.text(),
            self.ui.lineEdit_doseRelVol3.text(),
            self.ui.lineEdit_doseRelVol4.text(),
            self.ui.lineEdit_doseRelVol5.text()
        ]

        volAbsDoseValues = [
            self.ui.lineEdit_volAbsDose1.text(),
            self.ui.lineEdit_volAbsDose2.text(),
            self.ui.lineEdit_volAbsDose3.text(),
            self.ui.lineEdit_volAbsDose4.text(),
            self.ui.lineEdit_volAbsDose5.text()
        ]

        volRelDoseValues = [
            self.ui.lineEdit_volRelDose1.text(),
            self.ui.lineEdit_volRelDose2.text(),
            self.ui.lineEdit_volRelDose3.text(),
            self.ui.lineEdit_volRelDose4.text(),
            self.ui.lineEdit_volRelDose5.text()
        ]

        # Apologies for ugly code. I blame PyQt. :-)
        # This looks for any structures in the dose or volume list widgets
        doseRelVolStructs = [
            [
                self.ui.listWidget_doseRelVol1.item(i).text()
                for i in range(self.ui.listWidget_doseRelVol1.count())
            ],
            [
                self.ui.listWidget_doseRelVol2.item(i).text()
                for i in range(self.ui.listWidget_doseRelVol2.count())
            ],
            [
                self.ui.listWidget_doseRelVol3.item(i).text()
                for i in range(self.ui.listWidget_doseRelVol3.count())
            ],
            [
                self.ui.listWidget_doseRelVol4.item(i).text()
                for i in range(self.ui.listWidget_doseRelVol4.count())
            ],
            [
                self.ui.listWidget_doseRelVol5.item(i).text()
                for i in range(self.ui.listWidget_doseRelVol5.count())
            ]
        ]

        volAbsDoseStructs = [
            [
                self.ui.listWidget_volAbsDose1.item(i).text()
                for i in range(self.ui.listWidget_volAbsDose1.count())
            ],
            [
                self.ui.listWidget_volAbsDose2.item(i).text()
                for i in range(self.ui.listWidget_volAbsDose2.count())
            ],
            [
                self.ui.listWidget_volAbsDose3.item(i).text()
                for i in range(self.ui.listWidget_volAbsDose3.count())
            ],
            [
                self.ui.listWidget_volAbsDose4.item(i).text()
                for i in range(self.ui.listWidget_volAbsDose4.count())
            ],
            [
                self.ui.listWidget_volAbsDose5.item(i).text()
                for i in range(self.ui.listWidget_volAbsDose5.count())
            ]
        ]

        volRelDoseStructs = [
            [
                self.ui.listWidget_volRelDose1.item(i).text()
                for i in range(self.ui.listWidget_volRelDose1.count())
            ],
            [
                self.ui.listWidget_volRelDose2.item(i).text()
                for i in range(self.ui.listWidget_volRelDose2.count())
            ],
            [
                self.ui.listWidget_volRelDose3.item(i).text()
                for i in range(self.ui.listWidget_volRelDose3.count())
            ],
            [
                self.ui.listWidget_volRelDose4.item(i).text()
                for i in range(self.ui.listWidget_volRelDose4.count())
            ],
            [
                self.ui.listWidget_volRelDose5.item(i).text()
                for i in range(self.ui.listWidget_volRelDose5.count())
            ]
        ]

        # Remove any empty strings (from empty input boxes)
        doseRelVolValues = list(filter(None, doseRelVolValues))
        volAbsDoseValues = list(filter(None, volAbsDoseValues))
        volRelDoseValues = list(filter(None, volRelDoseValues))
        doseRelVolStructs = list(filter(None, doseRelVolStructs))
        volAbsDoseStructs = list(filter(None, volAbsDoseStructs))
        volRelDoseStructs = list(filter(None, volRelDoseStructs))

        # Convert all to numpy arrays. There's no real need for the first three
        # to be arrays instead of lists, but why not? Plus it saves converting
        # each element to floats individually.
        doseRelVolValues = np.array(doseRelVolValues, dtype='float')
        volAbsDoseValues = np.array(volAbsDoseValues, dtype='float')
        volRelDoseValues = np.array(volRelDoseValues, dtype='float')
        doseRelVolStructs = np.array(doseRelVolStructs)
        volAbsDoseStructs = np.array(volAbsDoseStructs)
        volRelDoseStructs = np.array(volRelDoseStructs)

        # # Quick sanity check
        # # The first three make sure each dose or volume widget has an associated
        # # quantity, and vice-versa
        # if doseRelVolValues.size != doseRelVolStructs.size:
        #     raise ValueError("Size mismatch: different number of dose")
        # if volAbsDoseValues.size != volAbsDoseStructs.size:
        #     raise ValueError("Size mismatch")
        # if volRelDoseValues.size != volRelDoseStructs.size:
        #     raise ValueError("Size mismatch")
        # # Structures in dose or volume widgets must be in the main widget (for
        # # "global" metrics) too.
        # if not np.all(np.isin(doseRelVolStructs.flatten(), finalStructs)):
        #     raise ValueError("Structures in dose to relative volume must also "
        #                      "be in main structure list.")
        # if not np.all(np.isin(volAbsDoseStructs.flatten(), finalStructs)):
        #     raise ValueError("Structures in volume to absolute dose must also "
        #                      "be in main structure list.")
        # if not np.all(np.isin(volRelDoseStructs.flatten(), finalStructs)):
        #     raise ValueError("Structures in volume to relative dose must also "
        #                      "be in main structures list")

        allMets = {}
        for struct in finalStructs:
            allMets[struct] = {}
            allMets[struct]['Globals'] = globalMets
            allMets[struct]['DoseRelVol'] = []
            allMets[struct]['VolAbsDose'] = []
            allMets[struct]['VolRelDose'] = []

        # Add in the specific metrics for each structure. Since there may
        # be multiple values wanted for the same structure and metrics (i.e.
        # D90 and D45 for Lung_L), make sure to append to a list for each
        # instead of overwriting.
        for i, structSet in enumerate(doseRelVolStructs):
            for struct in structSet:
                allMets[struct]['DoseRelVol'].append(doseRelVolValues[i])

        for i, structSet in enumerate(volAbsDoseStructs):
            for struct in structSet:
                allMets[struct]['VolAbsDose'].append(volAbsDoseValues[i])

        for i, structSet in enumerate(volRelDoseStructs):
            for struct in structSet:
                allMets[struct]['VolRelDose'].append(volRelDoseValues[i])

        # Get a list of all specific dose and volume metrics for later use
        # in the output spreadsheet. First item in each extend is the metric,
        # and the second is the actual volume or dose that corresponds to the
        # metric
        doseHeaders = []
        volADoseHeaders = []
        volRDoseHeaders = []
        for val in doseRelVolValues:
            doseLabel = 'D' + str(val)
            volumeLabel = 'True Vol at %s' % doseLabel
            doseHeaders.extend([doseLabel, volumeLabel])
        for val in volAbsDoseValues:
            volumeLabel = 'V' + str(val) + 'Gy'
            aDoseLabel = 'True Dose at %s' % volumeLabel
            volADoseHeaders.extend([volumeLabel, aDoseLabel])
        for val in volRelDoseValues:
            volumeLabel = 'V' + str(val) + '%'
            rDoseLabel = 'True Dose at %s' % volumeLabel
            volRDoseHeaders.extend([volumeLabel, rDoseLabel])

        return allMets, doseHeaders, volADoseHeaders, volRDoseHeaders

    def extractMetrics(self):
        """Extract all metrics and send to an Excel spreadsheet."""

        structsAndMets, doseHeaders, volADoseHeaders, volRDoseHeaders = \
            self.getStructsAndMets()
        structs = list(sorted(structsAndMets.keys(), key=str.lower))

        # "Global" mets are the same for all structures so it's okay to just use
        # the first occurrence
        globalMets = structsAndMets[structs[0]]['Globals']

        # Begin initial work for output spreadsheet
        rowsPerPlan = len(self.fileList) * len(structs)
        columns = ['File Name', 'Patient ID', 'Course', 'Plan', 'Structure']
        columns = columns + globalMets + doseHeaders + volADoseHeaders + \
            volRDoseHeaders
        dfMain = pd.DataFrame(
            data=np.full((rowsPerPlan, len(columns)), np.nan), columns=columns)

        # These keys correspond to the DoseRelVol, VolAbsDose, and VolRelDose
        # keys in structsAndMets dictionary, but are the "proper" names for the
        # column headers in the text file (which vary based on export params)
        absDoseKey = self.allMetNames[-3]
        relDoseKey = self.allMetNames[-2]
        ratioVolKey = self.allMetNames[-1]

        # We have to explicitly define the index (which corresponds to row
        # number) since a new row is created per patient, per structure.
        k = 0
        for plan in self.fileList:
            planInfo = readText(plan)
            dfMain.loc[k, 'File Name'] = plan
            dfMain.loc[k, 'Course'] = planInfo[structs[0]]['Course']
            dfMain.loc[k, 'Plan'] = planInfo[structs[0]]['Plan']

            for struct in structs:
                dfMain.loc[k, 'Patient ID'] = planInfo['Patient ID']
                dfMain.loc[k, 'Structure'] = struct
                structInfo = planInfo[struct]
                for glMet in globalMets:
                    dfMain.loc[k, glMet] = structInfo[glMet]

                # Find dose at the closest relative volume to the specified
                # relative volume
                if structsAndMets[struct]['DoseRelVol']:
                    for volMet in sorted(structsAndMets[struct]['DoseRelVol']):
                        label = 'D' + str(volMet)
                        volumes = structInfo[ratioVolKey]
                        ind = np.abs(volumes - volMet).argmin()
                        dose = structInfo[absDoseKey][ind]
                        dfMain.loc[k, label] = dose
                        dfMain.loc[k, 'True Vol at %s' % label] = volumes[ind]

                # Find volume at the closest absolute dose to the specified
                # absolute dose
                if structsAndMets[struct]['VolAbsDose']:
                    for doseMet in sorted(
                            structsAndMets[struct]['VolAbsDose']):
                        label = 'V' + str(doseMet) + 'Gy'
                        doses = structInfo[absDoseKey]
                        ind = np.abs(doses - doseMet).argmin()
                        volume = structInfo[ratioVolKey][ind]
                        dfMain.loc[k, label] = volume
                        dfMain.loc[k, 'True Dose at %s' % label] = doses[ind]

                # Find volume at the closest relative dose to the specified
                # relative dose
                if structsAndMets[struct]['VolRelDose']:
                    for doseMet in sorted(
                            structsAndMets[struct]['VolRelDose']):
                        label = 'V' + str(doseMet) + '%'
                        doses = structInfo[relDoseKey]
                        ind = np.abs(doses - doseMet).argmin()
                        volume = structInfo[ratioVolKey][ind]
                        dfMain.loc[k, label] = volume
                        dfMain.loc[k, 'True Dose at %s' % label] = doses[ind]
                k = k + 1

        outPath = os.path.join(self.pwd, 'Results', 'dvhVals.xlsx')
        book = results_book(outPath)
        writer = pd.ExcelWriter(outPath, engine='openpyxl')
        writer.book = book
        # Each patient gets a separate sheet. If this is running for a patient
        # that already exists in the workbook, give the option to create a new
        # sheet and add the current date and time instead of appending to the
        # existing sheet.
        sheetName = planInfo['Patient ID']
        if sheetName in writer.book.sheetnames:
            # Some prep work for pandas to support appending - see
            # https://github.com/pandas-dev/pandas/pull/21251
            # TODO: consider enabling when pandas reaches 0.24
            # title = 'Patient exists in database'
            # msg = 'Patient already exists in the spreadsheet.\nWould you ' \
            #       'like to append to existing data?\n\nIf not, a new sheet ' \
            #       'will be automatically created. '
            # retval = QMessageBox.question(self, title, msg, QMessageBox.Yes |
            #                               QMessageBox.No, QMessageBox.Yes)
            # if retval == QMessageBox.Yes:
            #     row = writer.book[sheetName].max_row + 5
            # else:
            sheetName = sheetName + '_' + datetime.now().strftime(
                '%Y%m%d%H%M%S')
        dfMain.to_excel(writer, index=False, sheet_name=sheetName)

        try:
            writer.save()
            print('Metrics were extracted to %s, sheet %s' % (outPath,
                                                              sheetName))
        except PermissionError:
            print('Unable to write results. Please close %s and try again.' %
                  outPath)


def main():
    """
    Start the GUI
    """
    if system() == 'Windows':  # Display the icon in the taskbar
        if 'Vista' not in platform():
            myappid = 'DVH Helper'
            ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(
                myappid)
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
    ex1 = Gui()
    ex1.setWindowTitle('DVH Helper')
    ex1.setWindowIcon(QtGui.QIcon('ic_assessment_black_48dp_2x'))
    ex1.show()
    ex1.activateWindow()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
