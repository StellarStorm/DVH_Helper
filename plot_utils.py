"""
Handles initial setup and various tasks for embedding a matplotlib window in
Qt
"""

import matplotlib as mplib
mplib.use('Qt5Agg')
from matplotlib.backends.backend_qt5agg import FigureCanvas
import matplotlib.style as mstyle
from matplotlib.figure import Figure
import numpy as np
from PyQt5 import QtWidgets


def setup_plot_for_qt(subplot_geom=[1, 1], axis_off=True, autoscale=True):
    """
    Create any number of plots inside a single canvas to be embedded in a Qt5
    window.

    Args:
        subplot_geom:       A 1x2 list for the number of rows and number of
                            columns to be set up. Valid entries of the list are
                            integers

        axis_off:           Whether to initialize the plot(s) with axes or not.
                            Defaults to True (axes will not be shown).

        autoscale:          Use autoscale


    Returns:
        figure:             An object of class matplotlib.Figure()
        axes:               Subplots created from figure.subplots. Geometry
                            is set by subplot_geom argument
        canvas:             A Figurecanvas() object with figure as an attribute.

    """
    figure = Figure()
    axes = figure.subplots(nrows=subplot_geom[0], ncols=subplot_geom[1])

    # Use if more than one subplot has been created
    if any(i > 1 for i in subplot_geom):
        if axis_off:
            np.vectorize(lambda ax: ax.set_axis_off())(axes)
        if autoscale:
            np.vectorize(
                lambda ax: ax.autoscale(enable=True, axis='both', tight=True))(
                    axes)
        elif not autoscale:
            np.vectorize(lambda ax: ax.autoscale(enable=False))(axes)
    # It's simpler when only one subplot is created
    else:
        if axis_off:
            axes.set_axis_off()
        if autoscale:
            axes.autoscale(enable=True, axis='both', tight=True)
        elif not autoscale:
            axes.autoscale(enable=False)

    canvas = FigureCanvas(figure)
    canvas.setSizePolicy(QtWidgets.QSizePolicy.Minimum,
                         QtWidgets.QSizePolicy.Minimum)
    canvas.updateGeometry()
    return figure, axes, canvas


def get_available_styles(exclude='_classic_test', sort=True):
    styles = mstyle.available
    if exclude:
        styles.remove(exclude)
    if sort:
        styles.sort(key=str.lower)
    return styles
