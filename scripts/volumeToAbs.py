"""
Convert % structure volume covered to absolute structure volume covered, when
the total structure volume (in absolute units) and the % structure volume
covered have already been found.

Requires:
1. A spreadsheet already containing volume in cc. This expects the output
of DVHelper.py
"""

import os
import numpy as np
import pandas as pd


def findVol(patientData, volID, mets, ignoreStructs=None):
    """
    Calculate the absolute volume(s) covered for all structures except those
    explicitly ignored.

    Note that this works on a blacklist principle -- all volumes are found
    EXCEPT for those structures which contain an string found in ignoreStructs
    in their name

    Args:
        patientData:    A pandas dataframe that must contain a column named
                        'Structure' and a column whose name is the same as
                        volID, plus one or more columns whose names are in mets.
        volID:          The column header in dataFile for the absolute, total
                        volume column
        mets:           A list of the volume metrics in percentage that should
                        be calculated in absolute dose
        ignoreStructs:   An optional list of strings that (partially) match
                        the name or names of any structures to be ignored

    Returns:
        patientData:    The same pandas dataframe as the input, but with
                        absolute volume columns added for each specified
                        volume metric.

    Example:
        1. Return the absolute volume covered for V95 and V98 for all
        structures in dataFrame.Structure, except structures whose names
        contain either 'PTV' or 'Lung'
        findVol(dataFrame, 'Volume [cmÂ³]', ['V95', 'V98'],
                ignoreStructs=['PTV', 'Lung'])
    """
    if ignoreStructs:
        structs = sorted([
            struct for struct in patientData.Structure
            if not any(skip in struct for skip in ignoreStructs)
        ])
    else:
        structs = sorted(patientData.Structure.values)
    # Structs will not be unique unless patientData only contains a single
    # plan
    structs = np.unique(structs)

    newMets = [met + '_abs' for met in mets]
    for struct in structs:
        volSeries = patientData.loc[patientData.Structure == struct, volID]
        for i, met in enumerate(mets):
            newMet = newMets[i]
            metSeries = patientData.loc[patientData.Structure == struct, met]
            patientData.loc[patientData.Structure == struct,
                            newMet] = volSeries * (metSeries / 100)
    return patientData


def main(dvhMetsPath, volumeMets, ignored):
    """
    Args:
        dvhMetsPath:    The file path to the DVH metrics spreadsheet
        volumeMets:     Volume metrics in percent coverage that are to be
                        converted to absolute coverages
        ignored:        A list of structures to ignore. Accepts partial matches.
    """
    dataFile = pd.ExcelFile(dvhMetsPath)
    saveFile = pd.ExcelWriter(dvhMetsPath, engine='openpyxl')

    ignoreSheets = [
        'All', 'README', 'RunInfo', ' HalcyonHN_19', ' HalcyonHN_20'
    ]
    activeSheets = [
        sheet for sheet in dataFile.sheet_names if sheet not in ignoreSheets
    ]

    for patientID in activeSheets:
        print('Calculating absolute volume coverage for ', patientID)
        patientDf = dataFile.parse(patientID)
        absVolDf = findVol(
            patientDf, 'Volume [cmÂ³]', volumeMets, ignoreStructs=ignored)
        absVolDf.to_excel(saveFile, index=False, sheet_name=patientID)

    # Make sure existing data in the ignored sheets are copied over.
    for skipped in ignoreSheets:
        if skipped in dataFile.sheet_names:
            print('Copying data from ', skipped)
            skippedDf = dataFile.parse(skipped)
            skippedDf.to_excel(saveFile, index=False, sheet_name=skipped)
    saveFile.save()


if __name__ == '__main__':
    volFile = os.path.abspath(
        'C:\\Users\\SGay1\\Box Sync\\HalcyonProject\\B\\Data\\dvhVals_raw.xlsx'
    )
    ignores = ['PTV']
    vols = ['V54.0Gy', 'V50.0Gy', 'V45.0Gy', 'V30.0Gy', 'V7.0Gy']
    main(volFile, vols, ignores)
