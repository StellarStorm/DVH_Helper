"""
TL:DR - get sheet ready for my R plotting script

Make a new "All" sheet that contains all normalized doses from the individual
sheets in ../Results/NormalizedDose.xlsx. Will still need some hand-tuning.
Also organize by error type... unmodified first, then random error, then
unmodified again, and finally systematic error.
"""
from datetime import datetime
import logging
import os
import sys
from openpyxl import load_workbook
from openpyxl.styles import Font
import pandas as pd
sys.path.append('..')
from utils import excel_run_info, logger

os.makedirs('../Logs', exist_ok=True)
# Set up log
logFile = os.path.abspath(os.path.join('../Logs/scripts.log'))
logger(os.path.abspath('../Logs'), 'scripts.log', 500)
logging.basicConfig(
    format='%(message)s', filename=logFile, level=logging.DEBUG)
logging.info(
    '\n========================================================================'
)
logging.info(datetime.now().strftime('%Y%m%d %H:%M:%S'))
logging.info(sys.argv[0])

doseFile = pd.ExcelFile(os.path.abspath('../Results/NormalizedDose.xlsx'))
logging.info('Input:\t%s', os.path.abspath('../Results/NormalizedDose.xlsx'))
if 'All' in doseFile.book.sheet_names():
    logging.error('ERROR: Sheet %s already exists.' % 'All')
    raise ValueError('Sheet %s already exists.' % 'All')

book = load_workbook('../Results/NormalizedDose.xlsx')
writer = pd.ExcelWriter('../Results/NormalizedDose.xlsx', engine='openpyxl')
writer.book = book  # Without this line, will be saved w/o existing info
allSheets = doseFile.book.sheet_names()
# Don't include information sheets
ignoreSheets = ['RunInfo', 'README']
activeSheets = [sheet for sheet in allSheets if sheet not in ignoreSheets]

allDf = pd.DataFrame()

for patient in activeSheets:
    print(patient)
    logging.info('Patient:\t%s', patient)
    patientDf = doseFile.parse(patient, header=0)

    # Keep consistent nomenclature for structures
    for struct in patientDf.columns[4:]:
        if struct == 'Cord+5mm':
            patientDf.rename(columns={'Cord+5mm': 'cord+5mm'}, inplace=True)
            logging.info('Renamed\t%s\tto\t%s' % (struct, 'cord+5mm'))
        elif all(i in struct
                 for i in ['Submand', 'L']) and struct != 'Submand_L':
            patientDf.rename(columns={struct: 'Submand_L'}, inplace=True)
            logging.info('Renamed\t%s\tto\t%s' % (struct, 'Submand_L'))
        elif all(i in struct
                 for i in ['Submand', 'R']) and struct != 'Submand_R':
            patientDf.rename(columns={struct: 'Submand_R'}, inplace=True)
            logging.info('Renamed\t%s\tto\t%s' % (struct, 'Submand_R'))
        elif 'PTV' in struct and '_' not in struct:
            a = struct.split('V')
            a.insert(1, 'V_')
            newStruct = ''.join(a)
            patientDf.rename(columns={struct: newStruct}, inplace=True)
            logging.info('Renamed\t%s\tto\t%s' % (struct, newStruct))

    unmodDf = patientDf[patientDf['Error Type'] == 'unmod']
    randDf = patientDf[patientDf['Error Type'] == 'rand']
    sysDf = patientDf[patientDf['Error Type'] == 'sys']
    reorderedDf = pd.concat(
        [unmodDf, randDf, unmodDf, sysDf], ignore_index=True)

    # This is relatively expensive and there's probably a better way to do
    # this
    allDf = pd.concat([allDf, reorderedDf], ignore_index=True, sort=False)

allDf.to_excel(writer, sheet_name='All', index=False)

if not 'RunInfo' in writer.book.sheetnames:
    # Ensure runtime info sheet is the first sheet
    writer.book.create_sheet('RunInfo', 0)
    writer.book['RunInfo'].append(['User', 'Date', 'Run Time', 'Script'])
    for cell in ['A1', 'B1', 'C1', 'D1']:
        writer.book['RunInfo'][cell].font = Font(bold=True)

writer.book['RunInfo'].append(excel_run_info(sys.argv[0]))
writer.save()
logging.info('Output:\t%s', os.path.abspath('../Results/NormalizedDose.xlsx'))
