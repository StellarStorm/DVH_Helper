"""
Combine all DVH metrics and gamma pass rates into a single sheet (for
stats analysis).
"""
import os
import sys
import numpy as np
import pandas as pd
sys.path.append('..')
from utils import results_book


def isolateGamma(gammaDf, gammaIndices):
    """
    Extract gamma pass rates from Pandas dataframe and return as a dictionary
    whose keys are gamma indices and values are pass rates.

    Args:
        gammaDf:        A pandas DataFrame containing at least the following:
                        (1) A "Test Indices (%/mm)" column
                        (2) A "Mean Percent Passed" column
                        Test indices should all be unique, and there should
                        only be one "Mean Percent Passed" value for each test
                        index

        gammaIndices:   A list of one or more identifiers used to indicate
                        gamma test indices. Should only contain values found in
                        the "Test Indices (%/mm)" column

    Returns:
        gammaVals:      A dictionary of gamma indices and pass rates

    Example:
    >>> gammaDf = pd.DataFrame({
        'Original Plan': ['Pt1', 'Pt1', 'Pt1', 'Pt1'],
        'Test Indices (%/mm)': ['3_3', '3_2', '2_2', '2_1'],
        'Mean Percent Passed': [99.93, 99.63, 98.43, 92.47]
    })

    >>> gammaIndices = ['3_3', '3_2', '2_1']

    >>> isolateGamma(gammaDf, gammaIndices)
    {'3_3': 99.933333, '3_2': 99.633333, '2_1' 92.466667}

    """

    gammaVals = {}

    for gamma in gammaIndices:
        gammaVals[gamma] = gammaDf.loc[
            gammaDf['Test Indices (%/mm)'] ==
            gamma, 'Mean Percent Passed'].values.tolist()[0]
    return gammaVals


def isolateDvhMetrics(dvhDf, structures, metrics):
    """
    Extract DVH metrics from a Pandas dataframe and return as a nested
    dictionary.
    whose (toplevel) keys are structures. Each structure sub-dictionary matches
    metrics (keys) with values.

    Args:
        dvhDf:          A pandas DataFrame of DVH structures and metrics
        structures:     A list of relevant DVH structures
        metrics:        A list of DVH metrics

    Returns:
        dvhVals:        A nested dictionary of DVH metrics. Each toplevel key
                        is a target structure whose value is a dictionary for
                        all metrics for that structure and their value.
    """

    dvhVals = {}
    for struct in structures:
        dvhVals[struct] = {}
        for met in metrics:
            metDf = dvhDf.loc[dvhDf.Metric == met]
            if not np.isnan(metDf[struct].values[0]):
                dvhVals[struct][met] = metDf[struct].values[0]
    return dvhVals


def dictToPandas(planInfo, metrics):
    """
    Create a pandas dataframe from a nested dictionary. Uses the (intermediate)
    output of main()

    Args:
        planInfo:       A nested dictionary created by combining the output of
                        isolateGamma() and isolateDvhMetrics() (plus a few other
                        keys if needed)
        metrics:        A list of all DVH metrics

    Returns:
        metDf:
    """

    cols = ['Original Plan', 'Error (mm)', 'Error Type'] + list(
        planInfo['Gamma'].keys()) + metrics

    metDf = pd.DataFrame(columns=cols, index=planInfo['DVH'].keys())
    for struct in planInfo['DVH'].keys():
        for met in metrics:
            try:
                val = planInfo['DVH'][struct][met]
                metDf.loc[struct, met] = val
            except KeyError:
                pass
    # Drop empty rows (structures that had no DVH metrics in the dictionary)
    metDf.dropna(how='all', inplace=True)
    for gamma in planInfo['Gamma'].keys():
        metDf[gamma] = planInfo['Gamma'][gamma]
    metDf['Original Plan'] = planInfo['Original Plan']
    metDf['Error (mm)'] = planInfo['Error (mm)']
    metDf['Error Type'] = planInfo['Error Type']
    return metDf


def main(gammaDf, dvhDf, ignoreMets):
    gammaIndices = gammaDf['Test Indices (%/mm)'].unique()
    plans = dvhDf.OriginalPlan.unique()
    errorValues = dvhDf['Error (mm)'].unique()
    metrics = [
        met.strip() for met in dvhDf.Metric.unique() if met not in ignoreMets
    ]
    structs = dvhDf.columns[4:].values.tolist()

    # Prep the output file
    book = results_book('../Results/Test.xlsx')
    writer = pd.ExcelWriter('../Results/Test.xlsx', engine='openpyxl')
    writer.book = book

    cols = ['Original Plan', 'Error (mm)', 'Error Type'
            ] + list(gammaIndices) + metrics
    newDf = pd.DataFrame(columns=cols)

    for plan in plans:
        planGamma = gammaDf.loc[gammaDf['Original Plan'] == plan.strip()]
        planDvh = dvhDf.loc[(dvhDf.OriginalPlan == plan)
                            & dvhDf.Metric.isin(metrics)]
        for err in errorValues:
            for errType in ['Random', 'Systematic']:
                if errType == 'Random':
                    errType2 = 'rand'
                elif errType == 'Systematic':
                    errType2 = 'sys'
                planDetails = {}
                planDetails['Original Plan'] = plan.strip()
                planDetails['Error (mm)'] = err
                planDetails['Error Type'] = errType

                errGamma = planGamma.loc[(planGamma['Error (mm)'] == err)
                                         &
                                         (planGamma['Error Type'] == errType)]
                errDvh = planDvh.loc[(planDvh['Error (mm)'] == err)
                                     & (planDvh['Error Type'] == errType2)]

                planDetails['Gamma'] = isolateGamma(errGamma, gammaIndices)

                planDetails['DVH'] = isolateDvhMetrics(errDvh, structs,
                                                       metrics)

                a = dictToPandas(planDetails, metrics)
                newDf = pd.concat([newDf, a])

    newDf.to_excel(writer, sheet_name='Combined')
    writer.save()


if __name__ == '__main__':
    gammaPath = os.path.abspath(
        'C:\\Users\\SGay1\\Box Sync\\HalcyonProject\\B\\Data\\GammaTest_AllMean.xlsx'
    )
    dvhMetsPath = os.path.abspath(
        'C:\\Users\\SGay1\\Box Sync\\HalcyonProject\\B\\Data\\Normalizeddose_CURATED.xlsx'
    )
    planInfoPath = os.path.abspath(
        'C:\\Users\\SGay1\\Box Sync\\HalcyonProject\\B\\Data\\VariousInfo.xlsx'
    )

    ignoreMets = [
        'NormDmin', 'NormDmod', 'NormD98', 'NormD95', 'NormDmed', 'V95.0%'
    ]

    gammaDf = pd.ExcelFile(gammaPath).parse('Curated')
    dvhDf = pd.ExcelFile(dvhMetsPath).parse('All')

    main(gammaDf, dvhDf, ignoreMets)
