"""
Find clinically significant deviations in a DVH spreadsheet (i.e. output of
normalizeMetrics.py) and isolate both structures/mets without clinically
significant errors, and those with, to a spreadsheet.
"""
import os
import sys
from openpyxl.styles import Font
import pandas as pd
sys.path.append('..')
from utils import results_book, excel_run_info


def find_cs(dataframe, structs, min_val=95, max_val=105):
    """
    For the given dataframe
        (a) find any metrics with values that are outside a provided range and
            replace with np.nan (returns metrics without clinically significant
            errors)
        (b) find any metrics with values that are inside a provided range and
            replace with np.nan (returns metrics with clinically significant
            errors)

    Args:
        dataframe:      A pandas dataframe that contains (at least) a "Metric"
                        column and one or more numerical data columns whose
                        headers are in the "structs" list argument

        structs:        A list of the headers of the numerical columns

        min_value:      The minimum acceptable value (lower bound) for the
                        numerical data found in columns with the structs header.
                        Defaults to 95.

        max_value:      The maximum acceptable value (upper bound) for the
                        numerical data found in columns with the structs header.
                        Defaults to 105.

    Returns:
        good_df:        A pandas dataframe with the same patient/plan/metric
                        identifiers as the provided dataframe argument that
                        contains only metrics and values that are within
                        bounds (no clinically significant errors)

        bad_df:         A pandas dataframe with the same patient/plan/metric
                        identifiers as the provided dataframe argument that
                        contains only metrics and values that are outside
                        bounds (clinically significant errors)
    """
    # Subset the ID columns (patient name, error type and size, metric...)
    df_ids = dataframe.loc[:, :'Metric']
    # Subset only the columns with values to check if they're within range.
    df_vals = dataframe[structs]
    # Every value that's not in range will be replaced by np.nan
    good_df_vals = df_vals.mask((df_vals < min_val) | (df_vals > max_val))
    # Every valut that's within range will be replaced by np.nan
    bad_df_vals = df_vals.where((df_vals < min_val) | (df_vals > max_val))

    good_df_vals.dropna(['rows', 'columns'], how='all', inplace=True)
    bad_df_vals.dropna(['rows', 'columns'], how='all', inplace=True)

    # Combine the identifiers with the dataframe of values within range.
    # Indexed so rows in df_ids that don't exist in good_df_vals aren't
    # added
    good_df = df_ids.loc[good_df_vals.index, :].join(good_df_vals)
    bad_df = df_ids.loc[bad_df_vals.index, :].join(bad_df_vals)

    return good_df, bad_df


def send_to_excel(dataframe, out_path, xl_sheet_name):
    """
    Helper function to write output data to excel spreadsheet.

    Args:
        dataframe:      A pandas dataframe
        out_path:       A string or os.path-style object for the path to the
                        spreadsheet. Accepts existing files; if the file does
                        not exist, it will be created.
        xl_sheet_name:  A string of the name for the sheet for data to be saved

    Returns:
        Nothing. If a return value is needed by another function, assume that
        this returns 'None', i.e. send_to_excel(...) == None is True
    """
    # Prep the output file
    book = results_book(out_path)
    writer = pd.ExcelWriter(out_path, engine='openpyxl')
    writer.book = book
    dataframe.to_excel(writer, index=False, sheet_name=xl_sheet_name)
    # Add run-time info
    if not 'RunInfo' in writer.book.sheetnames:
        # Ensure runtime info sheet is the first sheet
        writer.book.create_sheet('RunInfo', 0)
        writer.book['RunInfo'].append(
            ['User', 'Date', 'Run Time', 'Script', 'Sheet Written'])
        for cell in ['A1', 'B1', 'C1', 'D1', 'E1']:
            writer.book['RunInfo'][cell].font = Font(bold=True)
    writer.book['RunInfo'].append(
        excel_run_info(sys.argv[0]) + [xl_sheet_name])
    writer.save()


def main(dataframe, mets_to_structs, err_mags, err_types, dry_run=False):
    no_cse_df = pd.DataFrame().reindex_like(dataframe)
    cse_df = pd.DataFrame().reindex_like(dataframe)
    for etype in err_types:
        df_etype = dataframe.loc[dataframe['Error Type'] == etype, :]
        for esize in err_mags:
            df_esize = df_etype.loc[df_etype['Error (mm)'] == esize, :]
            for met in mets_to_structs:
                structs = [
                    'OriginalPlan', 'Error (mm)', 'Error Type', 'Metric'
                ] + mets_to_structs[met]

                df_structs = df_esize[structs].loc[df_esize.Metric == met, :]
                good_df, bad_df = find_cs(df_structs, mets_to_structs[met])

                # Save the new values at the correct indices for new_df
                # (yeah, it's important)
                no_cse_df.loc[good_df.index, good_df.columns] = good_df
                cse_df.loc[bad_df.index, bad_df.columns] = bad_df

    no_cse_df.dropna(['rows', 'columns'], how='all', inplace=True)
    cse_df.dropna(['rows', 'columns'], how='all', inplace=True)
    if not dry_run:
        os.makedirs('../Results', exist_ok=True)
        sheet_id = 'All'
        out_file = '../Results/clinic_sig_err.xlsx'
        send_to_excel(no_cse_df, out_file, sheet_id + '_no_err')
        send_to_excel(cse_df, out_file, sheet_id + '_with_err')


STRUCTS_FOR_METS = {
    'NormDmax': ['BrainStem', 'cord+5mm', 'SpinalCord'],
    'NormDmean': ['Parotid_R'],
    'NormV54.0Gy_abs': ['BrainStem'],
    'NormV45.0Gy_abs': ['cord+5mm', 'SpinalCord'],
    'NormV30.0Gy_abs': ['Parotid_R']
}

ERR_MAG = [3, 5, 7, 10]
ERR_TYPE = ['rand', 'sys']
FPATH = os.path.abspath(
    r'C:\Users\SGay1\Box Sync\HalcyonProject\B\Data\NormalizedDose_volnorm.xlsx'
)
DATA = pd.ExcelFile(FPATH).parse('5%')

if __name__ == '__main__':
    main(DATA, STRUCTS_FOR_METS, ERR_MAG, ERR_TYPE)
