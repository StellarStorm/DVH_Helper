# Scripts
These are single-purpose scripts that I wrote per-task. They're designed around
a particular problem so they won't work without tweaking based on your dataset.

- volumeToAbs.py  
  For my Halcyon project. This calculates the absolute volume covered for all
  specified structures and volume metrics, given a spreadsheet (the raw output
  from DVHelper.py) that contains  
    (a) the total absolute volume for all specified structures, and  
    (b) the percent volume covered for all specified structures

  The algorithm is pretty simple: multiply the absolute total volume by the
  percent volume covered to get the absolute covered volume.

  This should be run before the other scripts, if absolute covered volume is
  wanted.

- normalizeMetrics.py  
    For my Halcyon project. This normalizes the metrics of each structure in
    each modified plan to the dose of each structure in the corresponding
    original plan. Normalized dose is returned as a percentage. For example, if
    the Dmax for the Brainstem of a modified plan is 32 Gy, and it's 30 Gy for
    the original plan, this will return normalized Dmax to Brainstem as
    `(32/30)*100 =` 106.67.

- collateNormMetrics.py  
    Also for Halcyon project. This collects all the normalized metrics generated
    by normalizeMetrics.py into a single 'All' sheet and saves them in the same
    spreadsheet.

- mega.py
    Halcyon yet again. This combines the gamma pass rates and DVH metrics into
    a single sheet to make it easier to calculate correlation values.
