"""
Normalizes dose of modified plans to original. Assumes final plan in spreadsheet
is the original plan.
dvhInputFile and mets are hard-coded and should be adjusted per user case.
"""
from datetime import datetime
from itertools import chain, repeat
import logging
import os
import re
import sys
import numpy as np
from openpyxl.styles import Font
import pandas as pd
sys.path.append('..')
from utils import results_book, excel_run_info, logger

os.makedirs('../Logs', exist_ok=True)
os.makedirs('../Results', exist_ok=True)

# Set up log
logFile = os.path.join('../Logs', 'scripts.log')
logger(os.path.abspath('../Logs'), 'scripts.log', 500)
logging.basicConfig(
    format='%(message)s', filename=logFile, level=logging.DEBUG)
logging.info(
    '\n========================================================================'
)
logging.info(datetime.now().strftime('%Y%m%d %H:%M:%S'))
logging.info(sys.argv[0])

inFile = 'C:\\Users\\SGay1\\Box Sync\\HalcyonProject\\B\\Data\\dvhVals_raw.xlsx'
dvhInputFile = pd.ExcelFile(inFile)
logging.info('Input:\t%s', inFile)

mets = np.array([
    'Min Dose [Gy]', 'Max Dose [Gy]', 'Mean Dose [Gy]', 'Modal Dose [Gy]',
    'Median Dose [Gy]', 'D98.0', 'D95.0'
])
# These metrics will not be normalized but will be copied as-is into the new
# output spreadsheet
ignoreMets = np.array([
    'V54.0Gy_abs', 'V50.0Gy_abs', 'V45.0Gy_abs', 'V30.0Gy_abs', 'V7.0Gy_abs',
    'V95.0%', 'V0.95Targ', 'V0.98Targ'
])

newMets = [
    'NormDmin', 'NormDmax', 'NormDmean', 'NormDmod', 'NormDmed', 'NormD98',
    'NormD95'
]
ignoreNewMets = ignoreMets.tolist()

logging.info('Normalized Metrics:\t%r', mets)
logging.info('Non-normalized Metrics:\t%r', ignoreMets)

# Prep the output file
book = results_book('../Results/NormalizedDose.xlsx')
writer = pd.ExcelWriter('../Results/NormalizedDose.xlsx', engine='openpyxl')
writer.book = book

ignoreSheets = ['Info', 'README', ' HalcyonHN_19', ' HalcyonHN_20']
activeSheets = [
    sheet for sheet in dvhInputFile.sheet_names if sheet not in ignoreSheets
]

for patient in activeSheets:
    print(patient)
    logging.info('Patient:\t%s', patient.strip())
    patientDf = dvhInputFile.parse(patient)

    plans = patientDf.Plan[~pd.isnull(patientDf.Plan)].values.tolist()

    # Error val and type are determined from plan name (i.e. 3fld_3rand). Since
    # the original plan name won't contain the needed info, skip it and then
    # append it manually
    # The convention I used to separate number of fields from error value and
    # type varied between "_" and "-" so check for both
    if all('_' in name for name in plans[:-1]):
        splitter = '_'
    elif all('-' in name for name in plans[:-1]):
        splitter = '-'
    else:
        raise ValueError('Unable to determine plan error type.')
    # Find type and magnitude of error -- random 3mm or unmodified 0mm, for
    # example
    mods = [planName.split(splitter)[-1] for planName in plans][:-1]
    # Search for numeric values in the string
    errVals = [float(re.findall(r'\d+', x)[0]) for x in mods]
    errVals.append(0)
    # Search for non-numerics, should be 'rand' and 'sys'
    errTypes = [re.findall(r'\D+', x)[0] for x in mods]
    errTypes.append('unmod')
    # Now extend the error values and types so they each fill an entire
    # column. Each item is extended "in-place" so the first element is repeated
    # n times, then the second n times, and so on, where n is the number of
    # metrics
    errVals = list(
        chain.from_iterable(
            repeat(val, (mets.size + ignoreMets.size)) for val in errVals))
    errTypes = list(
        chain.from_iterable(
            repeat(typ, (mets.size + ignoreMets.size)) for typ in errTypes))

    infoHeader = ['OriginalPlan', 'Error (mm)', 'Error Type', 'Metric']
    plans = patientDf.Plan[~pd.isnull(patientDf.Plan.values)]
    structs = sorted(np.unique(patientDf.Structure), key=str.lower)
    filler = np.full([
        len(plans) * (mets.size + ignoreMets.size),
        len(structs) + len(infoHeader)
    ], np.nan)
    normDf = pd.DataFrame(data=filler, columns=infoHeader + structs)
    normDf['OriginalPlan'] = [patient
                              ] * (mets.size + ignoreMets.size) * len(plans)
    normDf['Error (mm)'] = errVals
    normDf['Error Type'] = errTypes

    for struct in structs:
        # Subset to include only metrics for the particular structure
        structDf = patientDf.loc[patientDf.Structure == struct]
        # Original plan values must be the final array.
        # normalisedDose will be an MxN data frame where M is the number of
        # plans (modfied and original) and N is the number of metrics, for the
        # particular structure
        normalisedDose = (structDf[mets] / structDf[mets].values[-1]) * 100
        ignored = structDf[ignoreMets]

        # If both the unmodified value and all modified values are zero, count
        # the normalized value as 100% (since no change) instead of reporting
        # as np.nan due to division by zero
        # Not that this won't run unless all values are zero -- cases where the
        # unmod is 0 and at least one unmod isn't will still return np.nan
        for met in normalisedDose.columns:
            if np.all(structDf[met] == 0):
                normalisedDose[met] = np.tile(100, normalisedDose[met].size)
                logging.info('Replaced 0 with 100%%:\t%s\t%s', struct, met)

        combined = np.concatenate(
            [normalisedDose.values, ignored.values], axis=1)

        normDf[struct] = combined.flatten()
        normDf['Metric'] = ((newMets + ignoreNewMets) * len(plans))

    # Make sure we don't overwrite results if for the same patient name as a
    # previous run.
    sheetName = patient
    if patient in writer.book.sheetnames:
        sheetName = patient + '_' + datetime.now().strftime('%Y%m%d%H%M%S')
        print('Sheet %s already exists. Data will be written to the %s sheet '
              'instead' % (patient, sheetName))
    normDf.to_excel(writer, index=False, sheet_name=sheetName)
    logging.info('Patient Normalised Metrics:\t%s', sheetName)

if not 'RunInfo' in writer.book.sheetnames:
    # Ensure runtime info sheet is the first sheet
    writer.book.create_sheet('RunInfo', 0)
    writer.book['RunInfo'].append(['User', 'Date', 'Run Time', 'Script'])
    for cell in ['A1', 'B1', 'C1', 'D1']:
        writer.book['RunInfo'][cell].font = Font(bold=True)
writer.book['RunInfo'].append(excel_run_info(sys.argv[0]))

writer.save()
logging.info('Output:\t%s', os.path.abspath('../Results/NormalizedDose.xlsx'))
