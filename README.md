# DVH Helper
[![Build Status](https://gitlab.com/StellarStorm/DVH_Helper/badges/master/pipeline.svg)](https://gitlab.com/StellarStorm/DVH_Helper/pipelines/)  
[![License](https://img.shields.io/badge/License-MIT-blue.svg)](LICENSE.md)  
Quickly [extract](#dvhelper) or [plot](#curveview) DVH metrics from text file
exports from treatment planning systems.


Preliminary: export DVH metrics to text files (Eclipse)
1. Open a patient in Eclipse's External Beam Planning workspace (QuickLinks >>
    Treatment Planning, External Beam Planning).
2. Under "Dose Statistics", select all structures you want. It's often faster to
    select all of them (even if you only want a few) since DVH Helper will let
    you select only the structures you need later.
    ![Eclipse view](images/EclipseDVH.png)
3. Right-click the DVH window and choose `Export DVH in Tabular Format`.
4. Repeat for any other plans you need.

<br />
<br />
<br />
<br />
<br />

## DVHelper:
Extract data with DVH Helper
1. In the same folder as this program, run `python DVHelper.py` or
`python3 DVHelper.py`. Please note that this program was written for Python 3
and probably will not work with python 2 without manually editing the source.

2. You'll be greeted with the DVH Helper interface. Click the `File Location`
button to navigate to the directory with the DVH text files.
Please note that you can select multiple files simultaneously - but only do this
**if the files are simply variations of each other.** This program determines
all possible structures by scanning a single text file. If all the selected
files are not variants of that file, it's likely that structures will vary
greatly between files, and the program will not be able to extract them
correctly.

For example, if you have modified PlanA four times, and then exported the DVH
values for all 5 (1 original + 4 modified) plans, you can safely select all five
and just run DVH Helper once.

However, if you have exported the DVH values for two or more very different
plans, you should select each file separately and run DVH Helper on each
individual plan.

![Selecting files](images/SelectFiles.png)
<br />
<br />
<br />
<br />
<br />
3. Next, drag-and-drop the structures you want to extract into the
  `Structures to extract` pane, and tick each type of measurement you want under
  the `Values` field. Two "types" of metrics are supported:  

    (a). "Absolute" metrics such as maximum or minimum dose, or total structure
    volume. See screenshot below for all supported metrics.

    (b). "Relative" metrics such as dose to 90% of a certain target volume. We
    support extracting dose to relative volume, volume at absolute dose, and
    volume at relative dose.
  ![Choose structures](images/FeatureSelect.png)

  <br />
  <br />
  <br />
  <br />
  <br />
4. Finally, click `Extract` and the values will be extracted into
  `Results/dvhVals.xlsx`.
  Each patient is automatically given their own sheet inside dvhVals.xlsx -
  ~~however, if a sheet with the patient's name already exists in the file due~~
  ~~to a previous run, you'll be given the option to~~
   ~~- Create a new sheet, named "patient name" + the current date and time, or~~
   ~~- Append the data to the pre-existing sheet.~~ *Temporarily disabled; may be
   re-enabled after pandas 0.24 is released.*


  | File Name              | Patient ID   | Course | Plan       | Structure | Max Dose [Gy] | Mean Dose [Gy] | D90.0 | True Vol at D90.0 | D98.0 | True Vol at D98.0 | V25.0Gy | True Dose at   V25.0Gy | V95.0%  | True Dose at   V95.0% |
  | ---------------------- | ------------ | ------ | ---------- | --------- | ------------- | -------------- | ----- | ----------------- | ----- | ----------------- | ------- | ---------------------- | ------- | --------------------- |
  | C:/demo/3fld_3rand.txt | demoPatient1 | DEMO   | 3fld_3rand | BrainStem | 31.638        | 10.166         |       |                   | 1.7   | 97.0072           | 8.68276 | 25                     |         |                       |
  |                        | demoPatient1 |        |            | cord+5mm  | 44.601        | 20.421         | 1.2   | 89.6724           | 0.8   | 97.3743           |         |                        |         |                       |
  |                        | demoPatient1 |        |            | PTV_6000  | 70.508        | 63.918         |       |                   |       |                   |         |                        | 99.383  | 94.9206               |
  | C:/demo/3fld_3sys.txt  | demoPatient2 | DEMO   | 3fld_3sys  | BrainStem | 31.917        | 9.772          |       |                   | 1.6   | 98.9264           | 6.11029 | 25                     |         |                       |
  |                        | demoPatient2 |        |            | cord+5mm  | 41.494        | 20.314         | 1.2   | 89.6046           | 0.8   | 97.3423           |         |                        |         |                       |
  |                        | demoPatient2 |        |            | PTV_6000  | 72.771        | 63.957         |       |                   |       |                   |         |                        | 94.3751 | 94.9206               |
  | C:/demo/3fld_5rand.txt | demoPatient3 | DEMO   | 3fld_5rand | BrainStem | 31.972        | 10.215         |       |                   | 1.7   | 97.5748           | 8.48655 | 25                     |         |                       |
  |                        | demoPatient3 |        |            | cord+5mm  | 44.269        | 20.6           | 1.2   | 89.846            | 0.8   | 97.5642           |         |                        |         |                       |
  |                        | demoPatient3 |        |            | PTV_6000  | 72.197        | 64.102         |       |                   |       |                   |         |                        | 98.726  | 94.9206               |
  | C:/demo/3fld_5sys.txt  | demoPatient4 | DEMO   | 3fld_5sys  | BrainStem | 32.29         | 9.696          |       |                   | 1.6   | 98.6654           | 4.75647 | 25                     |         |                       |
  |                        | demoPatient4 |        |            | cord+5mm  | 41.853        | 20.331         | 1.2   | 89.7409           | 0.8   | 97.4934           |         |                        |         |                       |
  |                        | demoPatient4 |        |            | PTV_6000  | 75.768        | 63.558         |       |                   |       |                   |         |                        | 84.7673 | 94.9206               |


## CurveView
View and compare DVH curves from up to four text files simultaneously.

1. Launch with `python CurveView.py` or `python3 CurveView.py` depending on how
python 3 is configured on your system.

2. Once the GUI is loaded, click the "Load Files" button and select between 1
and 4 DVH text files.
![Load Files](images/SelectFiles_CV.png)

3. Tick each structure to be plotted, and choose which metrics are to be
plotted on the X and Y axes.
Four line styles are used to distinguish patients in the plot:
  - solid
  - dashed
  - dash-dot
  - dotted

The order of plans in the "Plan Order" widget determines the line style of each
plan. To change the style, drag the plans within the widget to rearrange.
For example, if a certain plan is the ground truth and you want it to have a
solid line, drag it to the top of the list.

4. Finally, click "Plot" to show a graph of the structures.
![Plots](images/DVH_Plots.png)


CurveView supports changing plot parameters without having to restart the
program. Simply make the desired changes and the click "Plot" again. For
example, to remove a certain structure from the plot, just untick its entry and
then click "Plot".

You can also save the current plot as a png image by selecting "Save Plot" from
the "Tools" menu.


## Requirements
These are the versions I used. You may be able to use previous
versions.
 - Matplotlib 2.2.3
 - Numpy 1.14.5 (note that versions before 1.8 will **not** work)
 - Pandas 0.23
 - Python 3.6
 - PyQt 5.6
 - Openpyxl 2.5.9

### License:
Copyright (C) 2018 S. Gay. All non-Qt and non-PyQt code is provided under the
MIT license.

Qt- and PyQt-specific code is provided under the GPL due to upstream licensing.
This includes the following files:
  - CurveUi.py
  - CurveUi.ui
  - DVHui.py
  - DVHui.ui
