"""
Read through multiple DVH value text files (exported as tabular data from
Eclipse). Extract all *Dose* values for Esophagus, Lung*, and PTV to
extractVals.txt
"""

import os
from tkinter import Tk, filedialog


pwd = os.getcwd()
# Get the folder with the DVH text files
master = Tk().withdraw()
FN_2187 = filedialog.askdirectory()

structures = os.listdir(FN_2187)

outFile = os.path.join(pwd, 'Results', 'extractVals.txt')

with open(outFile, 'a') as resultFile:
    for struct in structures:
        # Add a header for each new plan
        resultFile.write('==================================================\n')
        resultFile.write(struct + '\n')
        # Full path for each structure text file
        target = os.path.join(FN_2187, struct)

        with open(target, 'r') as stats:
            contents = stats.readlines()
            # Plan IDs
            resultFile.write(contents[1])  # Patient ID
            resultFile.write(contents[11])  # Course
            resultFile.write(contents[10])  # Plan
            for i, line in enumerate(contents):

                if 'Esophagus' in line:
                    resultFile.write(line)
                    for item in contents[i:i+16]:
                        # Find and extract all dose values
                        if 'Dose' in item:
                            resultFile.write(item)
                    resultFile.write('\n')


                elif 'Lung' in line:
                    resultFile.write(line)
                    for item in contents[i:i+16]:
                        # Find and extract all dose values
                        if 'Dose' in item:
                            resultFile.write(item)
                    resultFile.write('\n')

                elif 'PTV' in line:
                    resultFile.write(line)
                    for item in contents[i:i+16]:
                        # Find and extract all dose values
                        if 'Dose' in item:
                            resultFile.write(item)
                    resultFile.write('\n')
