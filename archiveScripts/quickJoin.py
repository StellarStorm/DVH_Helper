'''
Quickly combine all the results from Results\extractVals.txt
'''

import os
import sys
import xlrd
from xlutils.copy import copy

structs = sys.argv[1:]

pwd = os.getcwd()
outTable = os.path.join(pwd, 'Results', 'dvhVals.xls')
inFile = os.path.join(pwd, 'Results', 'extractVals-DEMO.txt')

wb = xlrd.open_workbook(outTable)
sheet = wb.sheet_by_index(0)

rb = copy(wb)
ws = rb.get_sheet(0)
row = 0
col = 0
# Find the first empty row so we don't overwrite existing data
cell = sheet.cell(row, 0)
try:
    while cell.ctype != 6:
        row = row + 1
        cell = sheet.cell(row, 0)
except IndexError:
    pass


# with open(inFile, 'r') as messy:
#     contents = messy.readlines()
#     for i, line in enumerate(contents):
#         line = line.strip()
#         # Since there will be data from multiple plans in the text file, skip
#         # five rows in the csv so each subsequent run doesn't overwrite the last
#         # plan
#         if '=====' in line:
#             if i != 0:
#                 row = row + 7
#         elif line.endswith('.txt'):
#             ws.write(row, 0, line)
#         elif line.startswith('Patient ID'):
#             ws.write(row, 1, line.split(' ')[-1])
#         elif line.startswith('Course'):
#             ws.write(row, 2, line.split(' ')[-1])
#         elif line.startswith('Plan'):
#             ws.write(row, 3, line.split(' ')[-1])
#         elif line.startswith('Structure'):
#             if line.endswith('Esophagus'):
#                 ws.write(row, 4, line.split('Structure: ')[-1])
#                 ws.write(row, 5, contents[i + 1].split(' ')[-1])
#                 ws.write(row, 6, contents[i + 2].split(' ')[-1])
#                 ws.write(row, 7, contents[i + 3].split(' ')[-1])
#                 ws.write(row, 8, contents[i + 4].split(' ')[-1])
#                 ws.write(row, 9, contents[i + 5].split(' ')[-1])
#                 ws.write(row, 10, contents[i + 6].split(' ')[-1])
#             elif line.endswith('Lung_L'):
#                 ws.write(row + 1, 4, line.split('Structure: ')[-1])
#                 ws.write(row + 1, 5, contents[i + 1].split(' ')[-1])
#                 ws.write(row + 1, 6, contents[i + 2].split(' ')[-1])
#                 ws.write(row + 1, 7, contents[i + 3].split(' ')[-1])
#                 ws.write(row + 1, 8, contents[i + 4].split(' ')[-1])
#                 ws.write(row + 1, 9, contents[i + 5].split(' ')[-1])
#                 ws.write(row + 1, 10, contents[i + 6].split(' ')[-1])
#             elif line.endswith('Lung_R'):
#                 ws.write(row + 2, 4, line.split('Structure: ')[-1])
#                 ws.write(row + 2, 5, contents[i + 1].split(' ')[-1])
#                 ws.write(row + 2, 6, contents[i + 2].split(' ')[-1])
#                 ws.write(row + 2, 7, contents[i + 3].split(' ')[-1])
#                 ws.write(row + 2, 8, contents[i + 4].split(' ')[-1])
#                 ws.write(row + 2, 9, contents[i + 5].split(' ')[-1])
#                 ws.write(row + 2, 10, contents[i + 6].split(' ')[-1])
#             elif line.endswith('Lungs'):
#                 ws.write(row + 3, 4, line.split('Structure: ')[-1])
#                 ws.write(row + 3, 5, contents[i + 1].split(' ')[-1])
#                 ws.write(row + 3, 6, contents[i + 2].split(' ')[-1])
#                 ws.write(row + 3, 7, contents[i + 3].split(' ')[-1])
#                 ws.write(row + 3, 8, contents[i + 4].split(' ')[-1])
#                 ws.write(row + 3, 9, contents[i + 5].split(' ')[-1])
#                 ws.write(row + 3, 10, contents[i + 6].split(' ')[-1])
#             elif line.endswith('ZZZLung Opt'):
#                 ws.write(row + 4, 4, line.split('Structure: ')[-1])
#                 ws.write(row + 4, 5, contents[i + 1].split(' ')[-1])
#                 ws.write(row + 4, 6, contents[i + 2].split(' ')[-1])
#                 ws.write(row + 4, 7, contents[i + 3].split(' ')[-1])
#                 ws.write(row + 4, 8, contents[i + 4].split(' ')[-1])
#                 ws.write(row + 4, 9, contents[i + 5].split(' ')[-1])
#                 ws.write(row + 4, 10, contents[i + 6].split(' ')[-1])
#             elif line.endswith('PTV_Opt'):
#                 ws.write(row + 5, 4, line.split('Structure: ')[-1])
#                 ws.write(row + 5, 5, contents[i + 1].split(' ')[-1])
#                 ws.write(row + 5, 6, contents[i + 2].split(' ')[-1])
#                 ws.write(row + 5, 7, contents[i + 3].split(' ')[-1])
#                 ws.write(row + 5, 8, contents[i + 4].split(' ')[-1])
#                 ws.write(row + 5, 9, contents[i + 5].split(' ')[-1])
#                 ws.write(row + 5, 10, contents[i + 6].split(' ')[-1])
#             elif 'PTV' in line:
#                 ws.write(row + 6, 4, line.split('Structure: ')[-1])
#                 ws.write(row + 6, 5, contents[i + 1].split(' ')[-1])
#                 ws.write(row + 6, 6, contents[i + 2].split(' ')[-1])
#                 ws.write(row + 6, 7, contents[i + 3].split(' ')[-1])
#                 ws.write(row + 6, 8, contents[i + 4].split(' ')[-1])
#                 ws.write(row + 6, 9, contents[i + 5].split(' ')[-1])
#                 ws.write(row + 6, 10, contents[i + 6].split(' ')[-1])
#
# rb.save(outTable)
