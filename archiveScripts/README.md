Old scripts I used before writing the GUI. They're **very** much written for a
single task and shouldn't be used; I just want to keep them around for
reference. :-).