"""
Commonly used utilities.
"""
from datetime import datetime
from getpass import getuser
import os
import numpy as np
from openpyxl import load_workbook, Workbook


def readText(fileName, enc=None):
    """
    Convert a text file containing DVH metrics exported by a treatment planning
    software like Eclipse into a python dictionary.
    Developed against exports from Eclipe 15.0

    Args:
        fileName:   absolute path to a DVH text file export. Should be
                    a string or a similar object.
        enc:        a string specifying text file encoding. If not specified,
                    should default to the platform default.

    Returns:
        planInfo:   a dictionary of all DVH structures and metrics for
                    each structure. Each structure is a key of planInfo, whose
                    value is a sub-dictionary containing all metrics for that
                    structure.
                    Also contains 3 patient info keys: Patient Name, Patient ID,
                    and Plan

    Example:
                readText('/path/to/lungZZ32.txt', 'utf-8')
    """
    planInfo = {}

    with open(fileName, 'r', encoding=enc) as currentFile:
        try:
            lines = [line.strip() for line in currentFile]
        except UnicodeDecodeError:
            raise ValueError('Bad or unreadable DVH text file.')

    # Get patient/plan info
    for line in lines:
        if 'Patient Name' in line:
            planInfo['Patient Name'] = line.split(':')[1].strip()
        elif line.startswith('Patient ID'):
            planInfo['Patient ID'] = line.split(':')[1].strip()
        elif line.startswith('Plan:'):
            planInfo['Plan'] = line.split(':')[1].strip()
            break

    # Find a list of all structures and their beginning and ending
    # indices, and a list of the column headers and their beginning and
    # ending indices.
    colHeaders = []
    colBegins = []
    colEnds = []
    structs = []
    structBegins = []
    structEnds = []
    finalIndex = len(lines) - 1
    for i, line in enumerate(lines):
        if line.startswith('Structure: '):
            structBegins.append(i)
            structs.append(line.split('Structure: ')[1:][0])
        elif line.startswith('Gradient Measure ['):
            structEnds.append(i)
        elif line.startswith('Dose [') and lines[i - 1] == '':
            colBegins.append(i + 1)
            colHeaders = [item.strip() + ']' for item in line.split(']')][0:3]
        elif line == '' and i != finalIndex:
            if lines[i + 1].startswith('Structure: '):
                colEnds.append(i - 1)
        elif i == finalIndex:
            colEnds.append(i - 1)

    structBegins = np.array(structBegins)
    structEnds = np.array(structEnds)
    colBegins = np.array(colBegins)
    colEnds = np.array(colEnds[1:])  # First element is not valid

    # Now get a dictionary of all values for every metric, for
    # every structure. Includes column data.
    for i, structBegin in enumerate(structBegins):
        # Indices. Add 1 to ending integers so the last item isn't left
        # out when indexing
        structEnd = structEnds[i] + 1
        colBegin = colBegins[i]
        colEnd = colEnds[i] + 1
        structure = structs[i]
        colVals = lines[colBegin:colEnd]

        # The "easy" data like structure, minimum dose
        structVals = np.array(
            [line.split(': ') for line in lines[structBegin:structEnd]]).T
        structVals = structVals.tolist()
        # Try converting the values to floats
        for j, item in enumerate(structVals[1]):
            try:
                structVals[1][j] = float(item)
            except ValueError:
                pass
        structdict = dict(zip(structVals[0], structVals[1]))

        # The column data - dose to volume and vice-versa
        a = []
        for line in colVals:
            a.append(np.array(line.split(), dtype='float'))
        a = np.array(a)
        for j, key in enumerate(colHeaders):
            structdict[key] = a.T[j]

        planInfo[structure] = structdict

    if not planInfo:
        raise ValueError('Bad or unreadable DVH text file.')
    else:
        return planInfo


def results_book(path):
    """
    Open a existing spreadsheet for writing results, or create a new one
    if it doesn't exist

    Args:
        path:       A string or similar object of the path to an existing
                    spreadsheet or the name of the spreadsheet to exist.

    Returns:
        book:       An openpyxl workbook object

    Example:
        results_book('../Results/Output.xlsx')
    """
    try:
        book = load_workbook(path)
    except FileNotFoundError:
        book = Workbook()
        book.remove(book['Sheet'])

    return book


def excel_run_info(scriptName):
    """
    Finds run-time info of a script or module (primarily for writing to an
    information sheet in an output workbook)

    Args:
        scriptName:     A string containing the name of the script calling this
                        function. In most cases it should be easiest to provide
                        the output of sys.argv[0]

    Returns:
        runInfo:        A list of runtime info: username, current date, current
                        time, and the provided script name

    Example:
        excel_run_info(sys.argv[0])
    """
    now = datetime.now()
    date = now.strftime('%m-%d-%Y')
    time = now.strftime('%H:%M:%S')
    try:
        username = getuser()
    except ModuleNotFoundError:
        username = 'UNKNOWN USER'

    runInfo = [username, date, time, scriptName]

    return runInfo


def logger(logDir, logName, maxCount):
    """
    Handles basic log setup and maintenance, like creating new logs whenever
    the current log gets beyond a certain line count (to prevent the log
    from growing so large it's difficult to open or read).

    Args:
        logDir:     A string or similar object containing the path to the
                    directory in which logs are or will be stored
        logName:    A string of the name of the active log file
        maxCount:   An integer of the maximum number of lines the log should
                    contain. If there are more lines than this in the log file,
                    it will be renamed based on the first and last entry dates.

    Returns:
        None

    Example:
        logger(os.path.abspath('../Logs'), 'scripts.log', 500)
    """
    logFile = os.path.join(logDir, logName)
    try:
        with open(logFile, 'r+') as f:
            contents = f.readlines()
            lineCount = sum(1 for line in contents)

            if lineCount >= maxCount:
                dates = []
                # Read the time and dates for all runtimes
                for i, line in enumerate(contents):
                    if '==========' in line:
                        dates.append(contents[i + 1].strip())
                startDate = dates[0]
                endDate = dates[-1]
                # Add a "header" to the top of the file that contains start
                # and end dates
                header = 'Log covers %s to %s' % (startDate, endDate)
                contents.insert(0, header)
                f.seek(0)
                f.write(''.join(contents))
                # We have to explicitly close the file so that it can be
                # renamed
                f.close()

                # Rename the file. Change formatting of date and time to allow
                # writing to Windows' file system and others that reserve the
                # ':' character
                startDate = startDate.replace(' ', '_').replace(':', '.')
                endDate = endDate.replace(' ', '_').replace(':', '.')
                renLogName = 'scripts' + '_' + startDate + '_' + endDate + '.log'
                renLogFile = os.path.join(logDir, renLogName)
                os.rename(logFile, renLogFile)
                msg = ('Log file has reached maximum size and was renamed '
                       'to %s. \nNew information will now be logged in '
                       'scripts.log' % renLogName)
                print(msg)

    except FileNotFoundError:
        pass
