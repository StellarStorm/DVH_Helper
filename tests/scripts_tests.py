import sys
import unittest
import pandas as pd
sys.path.append('..')
from scripts import volumeToAbs


class TestAbsoluteVolumeConversions(unittest.TestCase):
    def setUp(self):
        self.target = volumeToAbs
        self.data = pd.read_csv('test_data/demo_data.csv')

    def test_conversion_to_absolute_vol_all_structs(self):
        """
        Check that conversion of percent volume covered of multiple structures
        is correctly converted to absolute volume covered.
        """
        expect = pd.read_csv('test_data/expect1.csv')
        out = self.target.findVol(self.data, 'AbsoluteVolume',
                                  ['V_1', 'V_2', 'V_3'])

        pd.util.testing.assert_frame_equal(out, expect)

    def test_conversion_to_absolute_vol_ignore_structs(self):
        """
        Check correct conversion of percent volume covered to absolute volume
        covered similarly to self.test_conversion_to_absolute_vol_all_structs,
        but also ensure that volumes of specified structures are excluded from
        conversion.
        """
        expect = pd.read_csv('test_data/expect2.csv')
        out = self.target.findVol(
            self.data,
            'AbsoluteVolume', ['V_1', 'V_2', 'V_3'],
            ignoreStructs=['R', 'OpticChiasm'])
        pd.util.testing.assert_frame_equal(out, expect)

    def tearDown(self):
        pass


if __name__ == '__main__':
    unittest.main()
