import unittest
import numpy as np
import pandas as pd


class DataIntegrity(unittest.TestCase):
    def setUp(self):
        pass

    def test_demo_data_csv(self):
        data = pd.read_csv('test_data/demo_data.csv')
        expectedCols = [
            'Plan', 'Structure', 'AbsoluteVolume', 'V_1', 'V_2', 'V_3'
        ]
        expectedShape = (20, 6)
        expectedMeans = [239.2925, 11.2625, 14.45, 5.8]

        outMeans = np.round(data.mean().values, decimals=4)

        self.assertListEqual(data.columns.values.tolist(), expectedCols)
        self.assertEqual(data.shape, expectedShape)
        self.assertListEqual(outMeans.tolist(), expectedMeans)

    def test_expect1_csv(self):
        data = pd.read_csv('test_data/expect1.csv')
        expectedCols = [
            'Plan', 'Structure', 'AbsoluteVolume', 'V_1', 'V_2', 'V_3',
            'V_1_abs', 'V_2_abs', 'V_3_abs'
        ]
        expectedShape = (20, 9)
        expectedMeans = [
            239.2925, 11.2625, 14.45, 5.8, 2.4146, 84.5893, 0.0115
        ]

        outMeans = np.round(data.mean().values, decimals=4)
        self.assertListEqual(data.columns.values.tolist(), expectedCols)
        self.assertEqual(data.shape, expectedShape)
        self.assertListEqual(outMeans.tolist(), expectedMeans)

    def test_expect2_csv(self):
        data = pd.read_csv('test_data/expect2.csv')
        expectedCols = [
            'Plan', 'Structure', 'AbsoluteVolume', 'V_1', 'V_2', 'V_3',
            'V_1_abs', 'V_2_abs', 'V_3_abs'
        ]
        expectedShape = (20, 9)
        expectedMeans = [239.2925, 11.2625, 14.45, 5.8, 4.8293, 4.2086, np.nan]

        outMeans = np.round(data.mean().values, decimals=4)
        self.assertListEqual(data.columns.values.tolist(), expectedCols)
        self.assertEqual(data.shape, expectedShape)
        # The final mean value is np.nan, which would fail direct comparison.
        self.assertListEqual(outMeans.tolist()[:-1], expectedMeans[:-1])
        self.assertTrue(np.isnan(outMeans[-1]))


if __name__ == '__main__':
    unittest.main()
