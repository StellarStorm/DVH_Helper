import os
import sys
import unittest
import numpy as np
sys.path.append('..')
from utils import readText


class TestTextReader(unittest.TestCase):
    """
    This provides some unit tests for the utils.readText module, which reads a
    dvh text file export (from Eclipse or another TPS) and extracts the data
    into a dictionary, which is then returned for use.
    Current tests:
    1.  All structures are loaded correctly and are the keys of the
        dictionary
    2.  The patient ID has been located correctly
    3.  Each structure should have the same set of metrics
    4.  Each metric is being loaded correctly (check by making sure the
        value type is the expected type)
    """

    def setUp(self):
        """
        Initialize test set
        """
        self.filename = os.path.abspath(
            os.path.join('test_data', 'demo_dvh.txt'))
        self.data = {}
        self.structs = []
        self.read_data()

    def read_data(self):
        """
        Load the data for other functions to test
        """
        self.data = readText(self.filename, 'utf-8')
        self.structs = list(self.data.keys())
        self.structs.remove('Patient Name')
        self.structs.remove('Patient ID')
        self.structs.remove('Plan')
        self.structs = sorted(self.structs)

    def test_structs(self):
        """
        Make sure the corect keys are being loaded. Three keys should be patient
        or plan indentifiers (the values removed from self.data in
        self.read_data() above) and the others should be structures.
        """
        expectKeyList = [
            'Patient Name', 'Patient ID', 'Plan', 'Submand_L', 'CTV_6000',
            'Eye_L', 'CTV_5400', 'Parotid_L', 'Eye_R', 'Parotid_R',
            'SpinalCord', 'PTV_6300', 'PTV_6000', 'PTV_5400', 'Mandible - PTV'
        ]
        expectKeyList = sorted(expectKeyList)
        expectStructsList = [
            'Submand_L', 'CTV_6000', 'Eye_L', 'CTV_5400', 'Parotid_L', 'Eye_R',
            'Parotid_R', 'SpinalCord', 'PTV_6300', 'PTV_6000', 'PTV_5400',
            'Mandible - PTV'
        ]
        expectStructsList = sorted(expectStructsList)
        outKeyList = sorted(list(self.data.keys()))
        self.assertListEqual(outKeyList, expectKeyList)
        self.assertListEqual(self.structs, expectStructsList)

    def test_id_ok(self):
        """
        Make sure the patient and plan identifiers are loaded correctly.
        """
        expectName = 'Demo_DVH, demo_dvh'
        expectID = 'Demo_DVH_11'
        expectPlan = 'demo_plan'
        self.assertEqual(self.data['Patient Name'], expectName)
        self.assertEqual(self.data['Patient ID'], expectID)
        self.assertEqual(self.data['Plan'], expectPlan)

    def test_struct_keys_ok(self):
        """
        Make sure each structure has the same set of metrics
        """
        expectStructKeysList = [
            'Structure', 'Approval Status', 'Plan', 'Course', 'Volume [cm³]',
            'Dose Cover.[%]', 'Sampling Cover.[%]', 'Min Dose [Gy]',
            'Max Dose [Gy]', 'Mean Dose [Gy]', 'Modal Dose [Gy]',
            'Median Dose [Gy]', 'STD [Gy]', 'Equiv. Sphere Diam. [cm]',
            'Conformity Index', 'Gradient Measure [cm]', 'Dose [Gy]',
            'Relative dose [%]', 'Ratio of Total Structure Volume [%]'
        ]
        expectStructKeysList = sorted(expectStructKeysList)
        for struct in self.structs:
            structKeysList = sorted(list(self.data[struct].keys()))
            self.assertListEqual(structKeysList, expectStructKeysList)

    def test_struct_mets_ok(self):
        """
        This makes sure that the correct data is being loaded for each
        structure by checking the type of each metric for each structure.

        Examples of failure would include a string or a float as one of the
        final three elements of a structure sub-dictionary instead of a
        numpy array.
        """

        expectedTypes = {
            'Structure': str,
            'Approval Status': str,
            'Plan': str,
            'Course': str,
            'Volume [cm³]': float,
            'Dose Cover.[%]': float,
            'Sampling Cover.[%]': float,
            'Min Dose [Gy]': float,
            'Max Dose [Gy]': float,
            'Mean Dose [Gy]': float,
            'Modal Dose [Gy]': float,
            'Median Dose [Gy]': float,
            'STD [Gy]': float,
            'Equiv. Sphere Diam. [cm]': float,
            'Conformity Index': str,
            'Gradient Measure [cm]': str,
            'Dose [Gy]': np.ndarray,
            'Relative dose [%]': np.ndarray,
            'Ratio of Total Structure Volume [%]': np.ndarray
        }
        for struct in self.structs:
            for met in self.data[struct].keys():
                self.assertIsInstance(self.data[struct][met],
                                      expectedTypes[met])

    def test_load_bad_files(self):
        """
        Ensure that utils.readText fails when provided a bad file

        If the file is a text file and contains certain keywords like
        Patient ID but is not a DVH file, readText() should fail with an index
        error. All other incorrect files should fail with ValueError.
        """
        self.assertRaises(IndexError, readText,
                          os.path.abspath('../CurveView.py'))
        self.assertRaises(ValueError, readText,
                          os.path.abspath('../CurveUi.py'))
        self.assertRaises(ValueError, readText,
                          os.path.abspath('../images/DVH_Plots.png'))


if __name__ == '__main__':
    unittest.main()
