import sys
import unittest
sys.path.append('..')
import plot_utils as putils


class TestPlotUtils(unittest.TestCase):
    def setUp(self):
        self.target = putils

    def test_singleplot_default(self):
        figure, axes, canvas = self.target.setup_plot_for_qt()

        self.assertTrue(axes.get_autoscale_on())
        self.assertTupleEqual(axes.get_geometry(), (1, 1, 1))
        self.assertTrue(figure == canvas.figure)

    def test_singleplot_kwargs(self):
        figure, axes, canvas = self.target.setup_plot_for_qt(autoscale=False)

        self.assertFalse(axes.get_autoscale_on())

    def test_multiplot_default(self):
        figure, axes, canvas = self.target.setup_plot_for_qt(
            subplot_geom=[1, 3])

        self.assertTupleEqual(axes.shape, (3, ))
        for i in range(0, 3):
            self.assertTrue(axes[i].get_autoscale_on())
            self.assertTupleEqual(axes[i].get_geometry(), (1, 3, i + 1))

    def test_multiplot_kwargs(self):
        figure, axes, canvas = self.target.setup_plot_for_qt(
            subplot_geom=[1, 3], autoscale=False)

        for i in range(0, 3):
            self.assertFalse(axes[i].get_autoscale_on())


if __name__ == '__main__':
    unittest.main()
