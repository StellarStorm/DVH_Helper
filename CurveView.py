"""
View DVH curves from tab-delimited DVH text file exports

Developed using exports from Varian Eclipse 15.0
"""
import ctypes
import os
from platform import platform, system
import sys
import matplotlib as mplib
import matplotlib.lines as mlines
import matplotlib.style as mstyle
from PyQt5 import QtCore, QtWidgets, QtGui
from CurveUi import Ui_MainWindow
from utils import readText
import plot_utils as putils


class Gui(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.model = QtGui.QStandardItemModel(self.ui.listView)

        self.ui.pushButton_loadDvhFile.clicked.connect(self.loadData)
        self.ui.pushButton_Plot.clicked.connect(self.plot)
        self.model.itemChanged.connect(self.getStructsToPlot)
        self.ui.actionSave_Plot.triggered.connect(self.savePlot)

        self.loadedFiles = []
        self.structsToPlot = []
        self.data = {}
        self.warnState = False

        mets = [
            'Dose [Gy]', 'Relative dose [%]',
            'Ratio of Total Structure Volume [%]'
        ]
        self.ui.comboBox_X.addItems(mets)
        self.ui.comboBox_Y.addItems(mets)

        self.createPlotCanvas()

    def createPlotCanvas(self):
        """
        Setup and add the plot canvas to the GUI.
        """
        self.figure, self._ax, self.canvas = putils.setup_plot_for_qt()
        self.ui.plotLayout.addWidget(self.canvas)
        styles = putils.get_available_styles()
        self.ui.comboBox_style.addItems(styles)
        if 'seaborn-white' in styles:
            self.ui.comboBox_style.setCurrentText('seaborn-white')
        elif 'classic' in styles:
            self.ui.comboBox_style.setCurrentText('classic')

    def loadData(self):
        """
        Select exported text file(s) and load data from it or them
        """
        # Make sure self.data is empty in case new file(s) are loaded without
        # first restarting the program
        self.data = {}
        files = QtWidgets.QFileDialog.getOpenFileNames(
            self, 'Select one or more DVH text file(s)')[0]
        self.loadedFiles = [os.path.abspath(fn) for fn in files]
        try:
            for fn in self.loadedFiles:
                self.data[fn] = readText(fn)
            # Display a warning about line style duplication if more than four files
            # are loaded
            if len(self.loadedFiles) > 4:
                self.warnOnOverload('files')
            self.updateUi()
        except (IndexError, ValueError):
            print('Bad or unreadable DVH file.')
            return

    def updateUi(self):
        """
        Update structure list and metrics in UI
        """
        self.ui.listWidget_planOrder.clear()
        # Assume all text files have the same structures
        tmpKey = list(self.data.keys())[0]
        plans = []
        for patient in self.data:
            plans.append(self.data[patient]['Plan'])
        self.ui.listWidget_planOrder.addItems(plans)

        # Get list of plan structures from dictionary keys
        remove_keys = ['Patient Name', 'Patient ID', 'Plan']
        structs = [i for i in self.data[tmpKey] if i not in remove_keys]
        structs = sorted(structs, key=str.lower)

        for i in structs:
            struct = QtGui.QStandardItem(i)
            struct.setFlags(QtCore.Qt.ItemIsUserCheckable
                            | QtCore.Qt.ItemIsEnabled)
            struct.setData(
                QtCore.QVariant(QtCore.Qt.Unchecked), QtCore.Qt.CheckStateRole)
            self.model.appendRow(struct)
        self.ui.listView.setModel(self.model)

    def getStructsToPlot(self, item):
        """
        Finds all checked structures to be plotted
        Some sort of Qt magic I don't understand...
        """
        if item.checkState() == QtCore.Qt.Checked:
            self.structsToPlot.append(item.text())
        if item.checkState() == QtCore.Qt.Unchecked:
            self.structsToPlot.remove(item.text())
        self.structsToPlot = sorted(self.structsToPlot, key=str.lower)
        if len(self.structsToPlot) > 10 and not self.warnState:
            self.warnOnOverload('structs')

    def warnOnOverload(self, warnType=None):
        """
        Provide a warning in a separate window if too many structures or files
        are selected.

        Args:
            warnType:       A string of the type of warning to issue. Valid
                            values are 'structs' and 'files'. If value is None
                            (default behavior) no warning will be issued.
        """
        if warnType == 'structs':
            warnItem = 'structures'
            maxNum = 10
            dupItem = 'colors'
            # Only allow ignoring additional warnings in the same session for
            # choosing more than the max structures.
            self.warnState = True
        elif warnType == 'files':
            warnItem = 'files'
            maxNum = 4
            dupItem = 'line styles'
        if warnType:
            msg = ('Too many %s selected.' % warnItem,
                   'Only %s %s are supported in the plot view.' % (maxNum,
                                                                   warnItem),
                   'If you select more than %s, some %s will be' %
                   (maxNum, warnItem), 'plotted with the same %s.' % dupItem)
            QtWidgets.QMessageBox.warning(self, warnItem, '\n'.join(msg))

    def getPlanOrder(self):
        """
        Read order of plans from GUI.
        Returns:
            orderedPlans:   A list of plan names in the order specified in the
                            GUI. Since the GUI supports re-arranging plan order
                            through drag-drop, this lets plans be associated
                            with a particular line style in the graph (for
                            example, a reference plan, if moved to the first
                            position in the UI, will be plotted with a solid
                            line while all other plans will use some sort of
                            dotted/dashed line.)
        """
        orderedPlans = [
            self.ui.listWidget_planOrder.item(i).text()
            for i in range(self.ui.listWidget_planOrder.count())
        ]
        return orderedPlans

    def plot(self):
        mplib.rcParams.update(mplib.rcParamsDefault)
        mstyle.use(self.ui.comboBox_style.currentText())
        self._ax.cla()
        xMet = self.ui.comboBox_X.currentText()
        yMet = self.ui.comboBox_Y.currentText()
        lines = []
        colors = {}
        lstyles = ['solid', 'dashed', 'dashdot', 'dotted']
        plans = self.getPlanOrder()
        # Since there are only 4 linestyles, repeat styles as needed to fit the
        # number of loaded plans
        if len(plans) > 4:
            repeatFactor = (len(plans) // 4) + 1
            lstyles = lstyles * repeatFactor
        linestyles = dict(zip(plans, lstyles))

        for i, fn in enumerate(self.loadedFiles):
            plan = self.data[fn]['Plan']
            for struct in self.structsToPlot:
                if i == 0:
                    line = self._ax.plot(
                        self.data[fn][struct][xMet],
                        self.data[fn][struct][yMet],
                        linestyle=linestyles[plan],
                        label=struct)
                    # Set the color for each structure so subsequent plots will
                    # use the same color
                    colors[struct] = line[0].get_color()
                else:
                    self._ax.plot(
                        self.data[fn][struct][xMet],
                        self.data[fn][struct][yMet],
                        color=colors[struct],
                        linestyle=linestyles[plan])
            line = mlines.Line2D([], [],
                                 color='grey',
                                 linestyle=linestyles[plan],
                                 label=plan)
            lines.append(line)

        self._ax.set_xlabel(xMet)
        self._ax.set_ylabel(yMet)
        # Legend to correlate color with structs
        structLegend = self._ax.legend(
            loc='center left',
            bbox_to_anchor=(1, 0.5),
            fancybox=True,
            shadow=True,
        )
        # Separate legend for correlating line style with plan
        lineLegend = self._ax.legend(
            handles=lines,
            loc='lower left',
            bbox_to_anchor=(1, 0.925),
        )
        self._ax.add_artist(structLegend)
        self._ax.add_artist(lineLegend)
        self._ax.figure.tight_layout()
        self._ax.figure.canvas.draw_idle()

    def savePlot(self):
        """
        Save image to file
        """
        saveName = QtWidgets.QFileDialog.getSaveFileName(
            self, 'Save Name', os.getcwd(), '.png')[0]
        self.figure.savefig(saveName)


def main():
    """
    Start the GUI
    """
    # Display the icon in the taskbar
    if system() == 'Windows' and 'Vista' not in platform():
        myappid = 'DVH Helper'
        ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
    ex1 = Gui()
    ex1.setWindowTitle('DVH Curve')
    ex1.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
