"""
Run all unit tests in test/ directory

From https://stackoverflow.com/a/40437679
"""
import unittest
loader = unittest.TestLoader()
start_dir = 'tests/'
suite = loader.discover(start_dir)

runner = unittest.TextTestRunner()
runner.run(suite)
